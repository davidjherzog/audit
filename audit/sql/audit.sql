-- RUN AS ENT_COMMON owner
CREATE TABLE ENT_COMMON.APP_AUDIT
(
  APP_AUDIT_ID      VARCHAR2(36)   NOT NULL,
  REQUESTOR_VALUE   VARCHAR2(30)   NULL,
--  REQUESTOR_VALUE_TYPE VARCHAR2(20) NULL,
  APPLICATION       VARCHAR2(30)   NULL,
  SERVICE_NAME 		VARCHAR2(250)  NULL,
  CATEGORY          VARCHAR2(20)   NOT NULL,
  SUB_CATEGORY      VARCHAR2(20)   NOT NULL,
  APP_SERVER_IP     VARCHAR2(100)  NULL,
  APP_SERVER_HOST   VARCHAR2(100)  NULL,
  CREATED_DATE      TIMESTAMP      NULL,
  MESSAGE           VARCHAR2(4000) NULL
)
;

ALTER TABLE ENT_COMMON.APP_AUDIT
  ADD CONSTRAINT APP_AUDIT_PK
     PRIMARY KEY (APP_AUDIT_ID);

CREATE INDEX ENT_COMMON.APP_AUDIT_CREATED_DATE_IDX
   ON ENT_COMMON.APP_AUDIT (CREATED_DATE)
;

CREATE INDEX ENT_COMMON.APP_AUDIT_APP_CAT_SUB_IDX
   ON ENT_COMMON.APP_AUDIT (APPLICATION, CATEGORY, SUB_CATEGORY)
;


-- AUXILIARY TABLE
CREATE TABLE ENT_COMMON.APP_AUDIT_AUX
(
  APP_AUDIT_AUX_ID  VARCHAR2(36) NOT NULL,
  APP_AUDIT_ID      VARCHAR2(36) NOT NULL,
  CREATED_DATE      TIMESTAMP NULL,
  REQUEST           CLOB NULL,
  RESPONSE          CLOB NULL,
  SERVICE_EXCEPTION CLOB NULL
)
;

ALTER TABLE ENT_COMMON.APP_AUDIT_AUX
  ADD CONSTRAINT APP_AUDIT_AUX_PK
     PRIMARY KEY (APP_AUDIT_AUX_ID);

ALTER TABLE ENT_COMMON.APP_AUDIT_AUX
   ADD CONSTRAINT APP_AUDIT_AUX_APP_AUDIT_ID_FK
      FOREIGN KEY (APP_AUDIT_ID)
      REFERENCES APP_AUDIT(APP_AUDIT_ID)
      ON DELETE CASCADE;

CREATE INDEX ENT_COMMON.APP_AUDIT_AUX_CRTD_DT_IDX
   ON ENT_COMMON.APP_AUDIT_AUX (CREATED_DATE)
;

CREATE OR REPLACE SYNONYM APP_AUDIT_SERVICE.APP_AUDIT FOR ENT_COMMON.APP_AUDIT;
CREATE OR REPLACE SYNONYM APP_AUDIT_SERVICE.APP_AUDIT_AUX FOR ENT_COMMON.APP_AUDIT_AUX;
