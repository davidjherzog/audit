package com.ljhsquared.audit.aspect;

import com.ljhsquared.audit.dtos.base.Request;
import com.ljhsquared.audit.dtos.base.Response;
import com.ljhsquared.audit.errorhandling.ApplicationError;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;

import java.util.Arrays;

/**
 * Aspect for entire application.
 * 
 * @author dherzog
 * 
 */
@Aspect
@Configurable()
public class ControllerAspect {

	private static Logger logger = LoggerFactory.getLogger(ControllerAspect.class.getName());
	
	@Value("${audit.auth}")
	private String auth; 
	
	/**
	 * Proxy method to log before and after service method calls.
	 * 
	 * @param joinPoint ProceedingJoinPoint
	 * @return object
	 * @throws Throwable throw exception
	 */
	@Around("execution(public * com.ljhsquared.audit.controller.*ControllerImpl.*(..))")
	public final Object logAround(final ProceedingJoinPoint joinPoint) throws Throwable {

		Request request = null;
		Response response = null;
		
		try {
			Object[] params = joinPoint.getArgs();
			request = (Request) params[0];
			validateBaseRequest(request);
			if (params.length > 1) {
				authenticate(params);
			}
			response = (Response) joinPoint.proceed();
			logger.debug("\n\n\t\t" 
					+ "The class " + joinPoint.getSignature().getDeclaringTypeName() 
					+ " with the method " + joinPoint.getSignature().getName() 
					+ "()" 
					+ " \n\t\t\t begins with " + Arrays.toString(joinPoint.getArgs()) 
					+ " \n\t\t\t ends with " + response + "\n");
		} catch (Exception e) {
			if (e instanceof ApplicationException) {
				logMethodException(joinPoint, e);
				if (logStackTrace((ApplicationException) e)) {
					logException((ApplicationException) e);
				}
			} else {
				logMethodException(joinPoint, e);
				e.printStackTrace();
			}
			throw e;
		}
		
		return response;
		
	}
	
	private void validateBaseRequest(final Request requestDTO) {
		// TODO test task
		if (requestDTO == null) {
			throw new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL));
		}

	}
	
	private void authenticate(final Object[] params) throws Exception {
		// second parameter will be HTTP Header for Authentication
		String userpass = "";
		if (params[1] instanceof String) {
			String[] authHeader = ((String) params[1]).split(" ");
			userpass = authHeader[1];
		}
		if (!userpass.equals(auth)) {
			throw new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_AUTHENTICATION_FAILED));
		} 	
		
	}
	
	private void logMethodException(final ProceedingJoinPoint joinPoint, final Exception e) {
		Object[] params = joinPoint.getArgs();
		logger.error("\n\n\t\t" 
				+ "Class " + joinPoint.getSignature().getDeclaringTypeName() 
				+ " with the method " + joinPoint.getSignature().getName() 
				+ "()" 
				+ " \n\t\t\t begins with " + params[0] 
				+ " \n\t\t\t and failed with the exception " + e + "\n");
	}
	
	private boolean logStackTrace(final ApplicationException exception) {
		return !exception.isSupressStackTrace();
	}

	private void logException(final ApplicationException e) {
		logger.error("Error Code: " + e.getError().getCode() + " - Message: " + e.getError().getMessage());
	}
}
