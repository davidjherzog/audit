package com.ljhsquared.audit.aspect;

import com.ljhsquared.audit.dtos.base.ErrorMessage;
import com.ljhsquared.audit.dtos.base.ErrorResponse;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

/**
 * @author dherzog
 *
 * Error Handler Aspect
 */
@ControllerAdvice
public class ErrorHandlerAspect {

    /**
     * Method that catches application errors
     *
     * @param request - HttpServletRequest
     * @param ae - Application Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(ApplicationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse applicationError(final HttpServletRequest request, final ApplicationException ae) {

    	ErrorResponse response = new ErrorResponse();
        
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(ae.getError().getCode());          
        errorMessage.setMessage(ae.getError().getMessage());

        response.setError(errorMessage);

        return response;
    }
    
    /**
     * Method that catches application errors
     *
     * @param request - HttpServletRequest
     * @param ve - Method Argument Not Valid Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
	public ErrorResponse validationError(final HttpServletRequest request,
			final MethodArgumentNotValidException ve) {

    	ErrorResponse response = new ErrorResponse();

        BindingResult result = ve.getBindingResult();
        List<ObjectError> objectErrors = result.getAllErrors();
        ObjectError objectError = objectErrors.get(0);
        
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(objectError.getDefaultMessage());
        errorMessage.setMessage(ErrorRegistry.get(objectError.getDefaultMessage()).getMessage());

        response.setError(errorMessage);

        return response;
    }
    
    /**
     * Method that catches sql errors that do not fall into the graceful error category
     *
     * @param request - HttpServletRequest
     * @param sqle - SQL Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(SQLException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse sqlError(final HttpServletRequest request, final SQLException sqle) {
        return serverError();
    }

    /**
     * Method that catches non application errors
     *
     * @param request - HttpServletRequest
     * @param e - Exception being thrown
     * @return - ErrorResponse
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse serverError(final HttpServletRequest request, final Throwable e) {
        e.printStackTrace();
        return serverError();
    }
    
    private ErrorResponse serverError() {
    	ErrorResponse response = new ErrorResponse();

    	ErrorMessage errorMessage = new ErrorMessage();
    	errorMessage.setCode(ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getCode());
    	errorMessage.setMessage(ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getMessage());

        response.setError(errorMessage);

        return response;
    }
}
