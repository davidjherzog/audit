package com.ljhsquared.audit.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;


/**
 * @author dherzog
 * 
 * Service Logging Aspect.
 *
 */
@Aspect
@Component
public class ServiceLoggingAspect {

	private static Logger logger = LoggerFactory.getLogger(ServiceLoggingAspect.class.getName());
	
	/**
	 * Proxy method to log before and after service method calls.
	 * 
	 * @param joinPoint ProceedingJoinPoint
	 * @return object
	 * @throws Throwable throw exception
	 */
	@Around("execution(public * com.ljhsquared.audit.service.*ServiceImpl.*(..))")
	public final Object logAround(final ProceedingJoinPoint joinPoint) throws Throwable {
		
		try {
			Object result = joinPoint.proceed();
			logger.debug("\n\n\t\t"
					+ "The class " + joinPoint.getSignature().getDeclaringTypeName() 
					+ " with the method " + joinPoint.getSignature().getName() 
					+ "()" 
					+ " \n\t\t\t begins with " + Arrays.toString(joinPoint.getArgs()) 
					+ " \n\t\t\t ends with " + result + "\n");
			return result;
		} catch (Exception e) {
			logger.error("\n\n\t\t" 
					+ "Class " + joinPoint.getSignature().getDeclaringTypeName() 
					+ " with the method " + joinPoint.getSignature().getName() 
					+ "()" 
					+ " \n\t\t\t begins with " + Arrays.toString(joinPoint.getArgs()) 
					+ " \n\t\t\t and failed with the exception " + e + "\n");
			throw e;
		}
	}

}