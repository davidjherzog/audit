package com.ljhsquared.audit.business.mapper;

import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.entities.EAuditClob;


/**
 * @author dherzog
 *
 * Audit Clob Mapper
 */
public interface AuditClobMapper {
	
	/**
	 * Map AuditClob to EAuditClob.
	 * 
	 * @param auditClobDTO AuditClob
	 * @return EAuditClob
	 * @throws Exception - exception to throw
	 */
	public EAuditClob mapFromDto(AuditClob auditClobDTO) throws Exception;
	
	/**
	 * Map AuditClob to EAuditClob.
	 * 
	 * @param auditClobDTO AuditClob
	 * @param auditClob AuditClob
	 * @return EAuditClob
	 * @throws Exception - exception to throw
	 */
	public EAuditClob mapFromDto(AuditClob auditClobDTO, EAuditClob auditClob) throws Exception;

}
