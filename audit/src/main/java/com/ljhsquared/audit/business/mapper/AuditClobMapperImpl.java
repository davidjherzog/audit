package com.ljhsquared.audit.business.mapper;

import java.util.UUID;
import org.springframework.stereotype.Component;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.entities.EAuditClob;


/**
 * @author dherzog
 *
 * Audit Clob Mapper Implementation
 */
@Component("auditClobMapper")
public class AuditClobMapperImpl implements AuditClobMapper {

	@Override
	public EAuditClob mapFromDto(final AuditClob auditClobDTO) throws Exception {
		return this.mapFromDto(auditClobDTO, new EAuditClob());
	}
	
	@Override
	public EAuditClob mapFromDto(final AuditClob auditClobDTO, final EAuditClob auditClob) throws Exception {
		
		auditClob.setId(UUID.randomUUID().toString());
		auditClob.setRequest(auditClobDTO.getRequest());
		auditClob.setResponse(auditClobDTO.getResponse());
		auditClob.setException(auditClobDTO.getException());
		
		return auditClob;
	}

}
