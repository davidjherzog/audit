package com.ljhsquared.audit.business.mapper;

import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.entities.EAudit;


/**
 * @author dherzog
 * 
 * Audit Mapper
 */
public interface AuditMapper {
	
	/**
	 * Map Audit to EAudit.
	 * 
	 * @param auditDTO Audit
	 * @return EAudit
	 * @throws Exception - exception to throw
	 */
	public EAudit mapFromDto(Audit auditDTO) throws Exception;
	
	/**
	 * Map Audit to EAudit.
	 * 
	 * @param auditDTO Audit
	 * @param audit EAudit
	 * @return EAudit
	 * @throws Exception - exception to throw
	 */
	public EAudit mapFromDto(Audit auditDTO, EAudit audit) throws Exception;

}
