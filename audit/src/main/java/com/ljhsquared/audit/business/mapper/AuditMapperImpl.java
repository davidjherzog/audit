package com.ljhsquared.audit.business.mapper;

import java.util.UUID;
import org.springframework.stereotype.Component;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.entities.EAudit;


/**
 * @author dherzog
 *
 * Audit Mapper Implementation
 */
@Component("auditMapper")
public class AuditMapperImpl implements AuditMapper {

	@Override
	public EAudit mapFromDto(final Audit auditDTO) throws Exception {
		return this.mapFromDto(auditDTO, new EAudit());
	}
	
	@Override
	public EAudit mapFromDto(final Audit auditDTO, final EAudit audit) throws Exception {
		
		audit.setId(UUID.randomUUID().toString());
		audit.setRequestor(auditDTO.getRequestor());
		audit.setApplication(auditDTO.getApplication());
		audit.setService(auditDTO.getService());
		audit.setCategory(auditDTO.getCategory());
		audit.setSubCategory(auditDTO.getSubCategory());
		audit.setAppServerIP(auditDTO.getAppServerIP());
		audit.setAppServerHost(auditDTO.getAppServerHost());
		audit.setMessage(auditDTO.getMessage());
		
		return audit;
	}

}
