package com.ljhsquared.audit.business.validator;


/**
 * @author dherzog
 * 
 * Validation constants.
 * 
 */
public class ValidConstants {

	// Audit
	
	// Requestor
	public static final int REQUESTOR_MIN_LENGTH = 0;
	public static final int REQUESTOR_MAX_LENGTH = 30;
	
	// Application
	public static final int APPLICATION_MIN_LENGTH = 0;
	public static final int APPLICATION_MAX_LENGTH = 20;
	
	// Service
	public static final int SERVICE_MIN_LENGTH = 0;
	public static final int SERVICE_MAX_LENGTH = 100;
		
	// Category
	public static final int CATEGORY_MIN_LENGTH = 0;
	public static final int CATEGORY_MAX_LENGTH = 20;
	
	// Sub-Category
	public static final int SUB_CATEGORY_MIN_LENGTH = 0;
	public static final int SUB_CATEGORY_MAX_LENGTH = 20;
	
	// Application Server IP
	public static final int APP_SERVER_IP_MIN_LENGTH = 0;
	public static final int APP_SERVER_IP_MAX_LENGTH = 100;
	
	// Application Server Host
	public static final int APP_SERVER_HOST_MIN_LENGTH = 0;
	public static final int APP_SERVER_HOST_MAX_LENGTH = 100;
	
	// Message
	public static final int MESSAGE_MIN_LENGTH = 0;
	public static final int MESSAGE_MAX_LENGTH = 4000;

	// Request
	//public static final int REQUEST_MIN_LENGTH = 0;
	//public static final int REQUEST_MAX_LENGTH = 300;
	
	// Response
	//public static final int RESPONSE_MIN_LENGTH = 0;
	//public static final int RESPONSE_MAX_LENGTH = 300;
	
	// Exception
	//public static final int EXCEPTION_MIN_LENGTH = 0;
	//public static final int EXCEPTION_MAX_LENGTH = 2;
	

}
