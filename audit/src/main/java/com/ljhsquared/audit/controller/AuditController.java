package com.ljhsquared.audit.controller;

import com.ljhsquared.audit.dtos.AuditCreateRequest;
import com.ljhsquared.audit.dtos.AuditCreateResponse;

/**
 * @author dherzog
 * 
 * Audit Controller Implementation
 *
 */
public interface AuditController {
	
	/**
	 * Create audit.
	 * 
	 * @param request - AuditCreateRequest
	 * @param authorization - String
	 * @return AuditCreateResponse
	 * @throws Exception - exception being thrown
	 */
	public AuditCreateResponse create(final AuditCreateRequest request, final String authorization) throws Exception;

}
