package com.ljhsquared.audit.controller;

import com.ljhsquared.audit.dtos.AuditCreateRequest;
import com.ljhsquared.audit.dtos.AuditCreateResponse;
import com.ljhsquared.audit.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;


/**
 * @author dherzog
 * 
 * Audit Controller Implementation
 *
 */
@Controller("auditController")
@RequestMapping("")
public class AuditControllerImpl implements AuditController {
	
	@Autowired
	private AuditService auditService;

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.PUT, 
			headers = { "Accept=application/json, application/xml", 
				"Content-type=application/json, application/xml" })
	@ResponseBody
	public final AuditCreateResponse create(@Valid @RequestBody final AuditCreateRequest request,
			@RequestHeader("Authorization") final String authorization) throws Exception {
		
		auditService.create(request.getAudit(), request.getAudit().getClob());
		
		return  new AuditCreateResponse();
	}

}
