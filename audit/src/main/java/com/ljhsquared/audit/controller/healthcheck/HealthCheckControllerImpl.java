package com.ljhsquared.audit.controller.healthcheck;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.ljhsquared.common.utils.List2HTML;
import com.ljhsquared.healthcheck.controller.HealthCheckController;
import com.ljhsquared.healthcheck.dtos.ConnectionCheckDTO;
import com.ljhsquared.healthcheck.dtos.NameValuePairDTO;
import com.ljhsquared.healthcheck.dtos.TableAccessCheckDTO;
import com.ljhsquared.healthcheck.service.HealthCheckService;

/**
 * @author dherzog
 * 
 * Health Check Controller.
 */
@Controller
//@RequestMapping("/healthcheck")
public class HealthCheckControllerImpl implements HealthCheckController {

	private static final String TITLE = "<title>Audit Health Checker</title>";
	private HealthCheckService healthCheckService;

	/**
	 * @param healthCheckService the healthCheckService to set
	 */
	@Autowired
	protected void setHealthCheckService(final HealthCheckService healthCheckService) {
		this.healthCheckService = healthCheckService;
	}

	@Override
	//@RequestMapping(value = "/database", method = RequestMethod.GET, produces = "text/html")
	//@ResponseBody
	public final String checkDatabase(final HttpServletRequest request) throws Exception {

		final StringBuilder builder = new StringBuilder();
		builder.append(List2HTML.getStyleSheet()).append(TITLE);

		final List<ConnectionCheckDTO> connectionChecks =
				healthCheckService.checkDataBaseConnections();
		builder.append(List2HTML.from(connectionChecks, ConnectionCheckDTO.class).getAsTable(
				"Database Connections"));

		final List<TableAccessCheckDTO> tableAccessChecks = healthCheckService.checkTableAccess();
		builder.append(List2HTML.from(tableAccessChecks, TableAccessCheckDTO.class).getAsTable(
				"Table Access"));

		return builder.toString();
	}

	@Override
	//@RequestMapping(value = "/build", method = RequestMethod.GET, produces = "text/html")
	//@ResponseBody
	public final String buildInfo(final HttpServletRequest request) throws Exception {

		final StringBuilder builder = new StringBuilder();
		builder.append(List2HTML.getStyleSheet()).append(TITLE);

		final List<NameValuePairDTO> values =
				healthCheckService.getBuildInfoFromManifest(request.getSession()
						.getServletContext());
		builder.append(List2HTML.from(values, NameValuePairDTO.class).getAsTable("Manifest"));

		return builder.toString();
	}

	/**
	 * Perform health check on connections - this method is for testability.
	 * @return List<ConnectionCheckDTO>
	 * @throws Exception - exception being thrown
	 */
	public final List<ConnectionCheckDTO> checkConnections() throws Exception {
		return healthCheckService.checkDataBaseConnections();
	}

	/**
	 * Perform health check on tables - this method is for testability.
	 * @return List<TableAccessCheckDTO>
	 * @throws Exception - exception being thrown
	 */
	public final List<TableAccessCheckDTO> checkTableAccess() throws Exception {
		return healthCheckService.checkTableAccess();
	}
	
}
