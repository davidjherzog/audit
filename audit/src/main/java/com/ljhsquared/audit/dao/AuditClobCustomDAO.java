package com.ljhsquared.audit.dao;

/**
 * @author dherzog
 * 
 * Audit Clob Custom DAO Interface
 */
public interface AuditClobCustomDAO {
	
	/**
	 * Purge Clob data
	 * 
	 * @param days - amount of days to keep 
	 * @throws Exception - exception to throw
	 */
	public void purge(final int days) throws Exception;

}
