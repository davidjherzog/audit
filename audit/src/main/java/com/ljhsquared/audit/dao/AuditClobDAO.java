package com.ljhsquared.audit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ljhsquared.audit.entities.EAuditClob;

/**
 * @author dherzog
 * 
 * Audit Clob Custom DAO Interface
 */
public interface AuditClobDAO extends JpaRepository<EAuditClob, String>, AuditClobCustomDAO {

}
