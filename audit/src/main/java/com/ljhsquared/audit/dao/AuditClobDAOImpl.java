package com.ljhsquared.audit.dao;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dherzog
 * 
 * Audit Clob DAO Implementation
 */
public class AuditClobDAOImpl implements AuditClobCustomDAO {
	
	private static Logger logger = LoggerFactory.getLogger(AuditClobDAOImpl.class.getName());
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public void purge(final int days) throws Exception {
		
		Date date = DateUtils.addDays(new Date(), days);
		
		Query query = entityManager.createNamedQuery("PURGE_CLOB");
		query.setParameter("createdDate", date);
		
		int records = query.executeUpdate();
		logger.info("Total auxilary records purged: " + records);
		
	}

}
