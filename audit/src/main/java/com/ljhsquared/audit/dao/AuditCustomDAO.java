package com.ljhsquared.audit.dao;

/**
 * @author dherzog
 * 
 * Audit Custom DAO Interface.
 *
 */
public interface AuditCustomDAO {
	
	/**
     * Count
     *
     * @param days - amount of days to count
     * @throws Exception - exception to throw
     * @return int
     */
    public int count(final int days) throws Exception;

}
