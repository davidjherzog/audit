package com.ljhsquared.audit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ljhsquared.audit.entities.EAudit;

/**
 * @author dherzog
 * 
 * Audit DAO Interface.
 *
 */
public interface AuditDAO extends JpaRepository<EAudit, String>, AuditCustomDAO {

}
