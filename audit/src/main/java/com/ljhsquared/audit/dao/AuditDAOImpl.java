package com.ljhsquared.audit.dao;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dherzog
 * 
 * Audit DAO Implementation
 */
public class AuditDAOImpl implements AuditCustomDAO {

    private static Logger logger = LoggerFactory.getLogger(AuditClobDAOImpl.class.getName());
    
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int count(final int days) throws Exception {
        Date date = DateUtils.addDays(new Date(), days);

        Query query = entityManager.createNamedQuery("COUNT");
        query.setParameter("createdDate", date);

        int count = ((Number) query.getSingleResult()).intValue();
        logger.info("Total record count: " + count);

        return count;
    }
}
