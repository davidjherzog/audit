package com.ljhsquared.audit.dtos;

import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import com.ljhsquared.audit.business.validator.ValidConstants;
import com.ljhsquared.audit.errorhandling.ErrorRegistry.ErrorConstants;

/**
 * @author dherzog
 * 
 * Audit DTO
 */
@XmlRootElement
public class Audit implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull(message = ErrorConstants.REQUESTOR_INVALID_LENGTH)
	@Size(min = ValidConstants.REQUESTOR_MIN_LENGTH, max = ValidConstants.REQUESTOR_MAX_LENGTH, 
			message = ErrorConstants.REQUESTOR_INVALID_LENGTH)
	private String requestor;
	
	@NotNull(message = ErrorConstants.APPLICATION_INVALID_LENGTH)
	@Size(min = ValidConstants.APPLICATION_MIN_LENGTH, max = ValidConstants.APPLICATION_MAX_LENGTH, 
			message = ErrorConstants.APPLICATION_INVALID_LENGTH)
	private String application;
	
	@NotNull(message = ErrorConstants.SERVICE_INVALID_LENGTH)
	@Size(min = ValidConstants.SERVICE_MIN_LENGTH, max = ValidConstants.SERVICE_MAX_LENGTH, 
			message = ErrorConstants.SERVICE_INVALID_LENGTH)
	private String service;
	
	@NotNull(message = ErrorConstants.CATEGORY_INVALID_LENGTH)
	@Size(min = ValidConstants.CATEGORY_MIN_LENGTH, max = ValidConstants.CATEGORY_MAX_LENGTH, 
			message = ErrorConstants.CATEGORY_INVALID_LENGTH)
	private String category;
	
	@NotNull(message = ErrorConstants.SUB_CATEGORY_INVALID_LENGTH)
	@Size(min = ValidConstants.SUB_CATEGORY_MIN_LENGTH, max = ValidConstants.SUB_CATEGORY_MAX_LENGTH, 
			message = ErrorConstants.SUB_CATEGORY_INVALID_LENGTH)
	private String subCategory;
	
	@NotNull(message = ErrorConstants.APP_SERVER_IP_INVALID_LENGTH)
	@Size(min = ValidConstants.APP_SERVER_IP_MIN_LENGTH, max = ValidConstants.APP_SERVER_IP_MAX_LENGTH, 
			message = ErrorConstants.APP_SERVER_IP_INVALID_LENGTH)
	private String appServerIP;
	
	@NotNull(message = ErrorConstants.APP_SERVER_HOST_INVALID_LENGTH)
	@Size(min = ValidConstants.APP_SERVER_HOST_MIN_LENGTH, max = ValidConstants.APP_SERVER_HOST_MAX_LENGTH, 
			message = ErrorConstants.APP_SERVER_HOST_INVALID_LENGTH)
	private String appServerHost;
	
	@NotNull(message = ErrorConstants.MESSAGE_INVALID_LENGTH)
	@Size(min = ValidConstants.MESSAGE_MIN_LENGTH, max = ValidConstants.MESSAGE_MAX_LENGTH, 
			message = ErrorConstants.MESSAGE_INVALID_LENGTH)
	private String message;

	@Valid
	@NotNull(message = ErrorConstants.AUDIT_CLOB_CANNOT_BE_NULL)
	private AuditClob clob;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}
	
	/**
	 * @return the requestor
	 */
	public String getRequestor() {
		return requestor;
	}
	
	/**
	 * @param requestor the requestor to set
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}
		
	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}
	
	/**
	 * @param service the service to set
	 */
	public void setService(final String service) {
		this.service = service;
	}

	/**
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(final String application) {
		this.application = application;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(final String category) {
		this.category = category;
	}

	/**
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}

	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(final String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @return the appServerIP
	 */
	public String getAppServerIP() {
		return appServerIP;
	}

	/**
	 * @param appServerIP the appServerIP to set
	 */
	public void setAppServerIP(final String appServerIP) {
		this.appServerIP = appServerIP;
	}
	
	/**
	 * @return the appServerHost
	 */
	public String getAppServerHost() {
		return appServerHost;
	}

	/**
	 * @param appServerHost the appServerHost to set
	 */
	public void setAppServerHost(final String appServerHost) {
		this.appServerHost = appServerHost;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * @return the clob
	 */
	public AuditClob getClob() {
		return clob;
	}

	/**
	 * @param clob the clob to set
	 */
	public void setClob(final AuditClob clob) {
		this.clob = clob;
	}

	@Override
	public String toString() {
		return String.format(
				"Audit [id=%s, requestor=%s, application=%s, category=%s, subCategory=%s, appServerIP=%s, "
						+ "appServerHost=%s, message=%s, clob=%s]", id, requestor, application, category, subCategory,
				appServerIP, appServerHost, message, clob);
	}

}
