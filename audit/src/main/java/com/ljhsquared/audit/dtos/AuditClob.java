package com.ljhsquared.audit.dtos;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author dherzog
 * 
 * AuditClob DTO
 */
@XmlRootElement
public class AuditClob implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String request;
	private String response;
	private String exception;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(final String request) {
		this.request = request;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(final String response) {
		this.response = response;
	}

	/**
	 * @return the exception
	 */
	public String getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(final String exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return String.format("AuditClob [id=%s, request=%s, response=%s, exception=%s]", id, request, response,
				exception);
	}

}
