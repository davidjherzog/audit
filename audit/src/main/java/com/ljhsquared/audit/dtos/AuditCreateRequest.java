package com.ljhsquared.audit.dtos;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import com.ljhsquared.audit.dtos.base.Request;
import com.ljhsquared.audit.errorhandling.ErrorRegistry.ErrorConstants;

/**
 * @author dherzog
 * 
 * Audit Create Request
 */
@XmlRootElement
public class AuditCreateRequest extends Request {

	private static final long serialVersionUID = 1L;
	
	@Valid
	@NotNull(message = ErrorConstants.AUDIT_CANNOT_BE_NULL)
	private Audit audit;

	/**
	 * @return the audit
	 */
	public Audit getAudit() {
		return audit;
	}

	/**
	 * @param audit the audit to set
	 */
	public void setAudit(final Audit audit) {
		this.audit = audit;
	}

	@Override
	public String toString() {
		return String.format("AuditCreateRequest [audit=%s]", audit);
	}
	
}
