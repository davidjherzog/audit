package com.ljhsquared.audit.dtos;

import javax.xml.bind.annotation.XmlRootElement;
import com.ljhsquared.audit.dtos.base.Response;


/**
 * @author dherzog
 * 
 * Audit Create Response
 */
@XmlRootElement
public class AuditCreateResponse extends Response {
	
	private static final long serialVersionUID = 1L;

}
