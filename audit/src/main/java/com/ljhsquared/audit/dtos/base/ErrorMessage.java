package com.ljhsquared.audit.dtos.base;

import java.io.Serializable;

/**
 * @author dherzog
 *
 * Error Message
 */
public class ErrorMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("ErrorMessage [code=%s, message=%s]",
                code, message);
    }
}

