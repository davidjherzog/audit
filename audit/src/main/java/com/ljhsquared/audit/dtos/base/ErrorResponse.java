package com.ljhsquared.audit.dtos.base;

import java.io.Serializable;


/**
 * @author dherzog
 *
 * Error Response 2
 */
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private ErrorMessage error;
	
	public ErrorMessage getError() {
		return error;
	}
	
	public void setError(final ErrorMessage error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return String.format("ErrorResponse2 [error=%s, toString()=%s]", error, super.toString());
	}
    
}
