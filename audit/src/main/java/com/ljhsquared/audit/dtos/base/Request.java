package com.ljhsquared.audit.dtos.base;

import java.io.Serializable;

/**
 * @author dherzog
 * 
 * Abstract Request DTO.
 * 
 */
public abstract class Request implements Serializable {

	private static final long serialVersionUID = 1L;
	
}
