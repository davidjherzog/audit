package com.ljhsquared.audit.dtos.base;

import java.io.Serializable;

/**
 * @author dherzog
 * 
 * Abstract Response DTO.
 * 
 */
public abstract class Response implements Serializable {

	private static final long serialVersionUID = 1L;

}
