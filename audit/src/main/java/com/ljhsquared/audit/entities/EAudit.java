package com.ljhsquared.audit.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * @author dherzog
 *
 * Audit
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "COUNT", query = "select count(a) from EAudit a "
                + "where a.createdDate <= :createdDate") })
@Table(name = "APP_AUDIT")
public class EAudit implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String requestor;
	private String application;
	private String service;
	private String category;
	private String subCategory;
	private String appServerIP;
	private String appServerHost;
	private String message;
	private Date createdDate;

	/**
	 * @return the id
	 */
	@Id
	@Column(name = "APP_AUDIT_ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}
	
	/**
	 * @return the requestor
	 */
	@Column(name = "REQUESTOR_VALUE", nullable = true, length = 30)
	public String getRequestor() {
		return requestor;
	}
	
	/**
	 * @param requestor the requestor to set
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}
	
	/**
	 * @return the service
	 */
	@Column(name = "SERVICE_NAME", nullable = true, length = 100)
	public String getService() {
		return service;
	}
	
	/**
	 * @param service the service to set
	 */
	public void setService(final String service) {
		this.service = service;
	}

	/**
	 * @return the application
	 */
	@Column(name = "APPLICATION", nullable = true, length = 30)
	public String getApplication() {
		return application;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(final String application) {
		this.application = application;
	}

	/**
	 * @return the category
	 */
	@Column(name = "CATEGORY", nullable = false, length = 20)
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(final String category) {
		this.category = category;
	}

	/**
	 * @return the subCategory
	 */
	@Column(name = "SUB_CATEGORY", nullable = false, length = 20)
	public String getSubCategory() {
		return subCategory;
	}

	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(final String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @return the appServerIP
	 */
	@Column(name = "APP_SERVER_IP", nullable = true, length = 100)
	public String getAppServerIP() {
		return appServerIP;
	}

	/**
	 * @param appServerIP the appServerIP to set
	 */
	public void setAppServerIP(final String appServerIP) {
		this.appServerIP = appServerIP;
	}
	
	/**
	 * @return the appServerHost
	 */
	@Column(name = "APP_SERVER_HOST", nullable = true, length = 100)
	public String getAppServerHost() {
		return appServerHost;
	}

	/**
	 * @param appServerHost the appServerHost to set
	 */
	public void setAppServerHost(final String appServerHost) {
		this.appServerHost = appServerHost;
	}

	/**
	 * @return the message
	 */
	@Column(name = "MESSAGE", nullable = true, length = 4000)
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * @return the createdDate
	 */
	@Column(name = "CREATED_DATE", nullable = true)
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}
	
	/**
	 * Set create date on persist.
	 */
	@PrePersist
	protected void onCreate() {
		Date date = new Date();
		createdDate = date;
	}

	@Override
	public String toString() {
		return String
				.format("EAudit [id=%s, requestor=%s, application=%s, category=%s, subCategory=%s, appServerIP=%s, "
						+ "appServerHost=%s, message=%s, createdDate=%s]",
						id, requestor, application, category, subCategory, appServerIP, appServerHost, message,
						createdDate);
	}
	
}
