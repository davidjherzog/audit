package com.ljhsquared.audit.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * @author dherzog
 *
 * Audit Clob
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "PURGE_CLOB", query = "delete from EAuditClob c "
			+ "where c.createdDate <= :createdDate") })
@Table(name = "APP_AUDIT_AUX")
public class EAuditClob implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String auditId;
	private Date createdDate;
	private String request;
	private String response;
	private String exception;

	/**
	 * @return the id
	 */
	@Id
	@Column(name = "APP_AUDIT_AUX_ID", nullable = false, length = 36)
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}
	
	/**
	 * @return the auditId
	 */
	@Column(name = "APP_AUDIT_ID", nullable = false, length = 36)
	public String getAuditId() {
		return auditId;
	}
	
	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(final String auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the createdDate
	 */
	@Column(name = "CREATED_DATE", nullable = true)
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}
	
	/**
	 * @return the request
	 */
	@Lob
	@Column(name = "REQUEST", nullable = true)
	public String getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(final String request) {
		this.request = request;
	}

	/**
	 * @return the response
	 */
	@Lob
	@Column(name = "RESPONSE", nullable = true)
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(final String response) {
		this.response = response;
	}
	
	/**
	 * @return the exception
	 */
	@Lob
	@Column(name = "SERVICE_EXCEPTION", nullable = true)
	public String getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(final String exception) {
		this.exception = exception;
	}
	
	/**
	 * Set create date on persist.
	 */
	@PrePersist
	protected void onCreate() {
		Date date = new Date();
		createdDate = date;
	}

	@Override
	public String toString() {
		return String.format("EAuditClob [id=%s, auditId=%s, createdDate=%s, request=%s, response=%s, exception=%s]",
				id, auditId, createdDate, request, response, exception);
	}	
	
}
