package com.ljhsquared.audit.errorhandling;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author dherzog
 * 
 * Error - Class that holds error code and message when exception thrown.
 * 
 *
 */
@XmlRootElement
public class ApplicationError implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code;
	private String message;

	/**
	 * Application Error Constructor.
	 */
	public ApplicationError() {
		
	}
	
	/**
	 * @param errorRegistry - ErrorRegistry
	 */
	public ApplicationError(final ErrorRegistry errorRegistry) {
		code = errorRegistry.getCode();
		message = errorRegistry.getMessage();
	}

	/**
	 * @param errorRegistry - ErrorRegistry
	 * @param message - Error message that will override the default message from
	 *            ErrorRegistry
	 */
	public ApplicationError(final ErrorRegistry errorRegistry, final String message) {
		this(errorRegistry, message, false);
	}

	/**
	 * @param errorRegistry - ErrorRegistry
	 * @param message - Error message that will override the default message from
	 *            ErrorRegistry
	 * @param appendToRegistryMessage - if true will append errorMessage to default message
	 */
	public ApplicationError(final ErrorRegistry errorRegistry, final String message,
			final boolean appendToRegistryMessage) {
		code = errorRegistry.getCode();
		if (appendToRegistryMessage) {
			this.message = errorRegistry.getMessage() + " " + message;
		} else {
			this.message = message;
		}
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return String.format("Error [code=%s, message=%s]", code, message);
	}
	
}
