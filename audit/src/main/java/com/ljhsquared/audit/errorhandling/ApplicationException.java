package com.ljhsquared.audit.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Card Connect Application Runtime Exception.
 * 
 * @author dherzog
 *
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ApplicationException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private ApplicationError error;
	private boolean supressStackTrace;

	/**
	 * 
	 */
	public ApplicationException() {
		super();
	}

	/**
	 * @param s - String
	 * @param throwable - Throwable
	 */
	public ApplicationException(final String s, final Throwable throwable) {
		super(s, throwable);
	}

	/**
	 * @param s - String
	 */
	public ApplicationException(final String s) {
		super(s);
	}

	/**
	 * @param throwable - Throwable
	 */
	public ApplicationException(final Throwable throwable) {
		super(throwable);
	}

	/**
	 * @param error - SpError
	 */
	public ApplicationException(final ApplicationError error) {
		super(error.getCode() + " : " + error.getMessage());
		setError(error);
	}

	/**
	 * @param error - Error
	 * @param supressStackTrace - suppress stack trace
	 */
	public ApplicationException(final ApplicationError error, final boolean supressStackTrace) {
		this(error);
		this.supressStackTrace = supressStackTrace;
	}
	
	/**
	 * @param error - Error
	 * @param throwable - Throwable
	 */
	public ApplicationException(final ApplicationError error, final Throwable throwable) {
		super(error.getCode() + " : " + error.getMessage(), throwable);
		setError(error);
	}

	/**
	 * @return the error
	 */
	public ApplicationError getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(final ApplicationError error) {
		this.error = error;
	}

	/**
	 * @return the supressStackTrace
	 */
	public boolean isSupressStackTrace() {
		return supressStackTrace;
	}
	
}
