package com.ljhsquared.audit.errorhandling;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dherzog
 * 
 * This enum is a place holder for all the application specific error codes and error messages.
 * 
 */
public enum ErrorRegistry {

	// Application specific errors 
	GENERAL_SERVICE_EXCEPTION(ErrorConstants.GENERAL_SERVICE_EXCEPTION, "General service exception."),
	SERVICE_AUTHENTICATION_FAILED(ErrorConstants.SERVICE_AUTHENTICATION_FAILED, "The service authentication failed."),
	SERVICE_REQUEST_IS_NULL(ErrorConstants.SERVICE_REQUEST_IS_NULL, "The service request cannot be null."),
	AUDIT_CANNOT_BE_NULL(ErrorConstants.AUDIT_CANNOT_BE_NULL, "Audit cannot be null."),
	AUDIT_CLOB_CANNOT_BE_NULL(ErrorConstants.AUDIT_CLOB_CANNOT_BE_NULL, "Audit Clob cannot be null."),
	REQUESTOR_INVALID_LENGTH(ErrorConstants.REQUESTOR_INVALID_LENGTH, "Requestor has invalid length."),
	APPLICATION_INVALID_LENGTH(ErrorConstants.APPLICATION_INVALID_LENGTH, "Application has invalid length."),
	SERVICE_INVALID_LENGTH(ErrorConstants.SERVICE_INVALID_LENGTH, "Service has invalid length."),
	CATEGORY_INVALID_LENGTH(ErrorConstants.CATEGORY_INVALID_LENGTH, "Category has invalid length."),
	SUB_CATEGORY_INVALID_LENGTH(ErrorConstants.SUB_CATEGORY_INVALID_LENGTH, "Sub-Category has invalid length."),
	APP_SERVER_IP_INVALID_LENGTH(ErrorConstants.APP_SERVER_IP_INVALID_LENGTH,
			"Application Server IP has invalid length."),
	APP_SERVER_HOST_INVALID_LENGTH(ErrorConstants.APP_SERVER_HOST_INVALID_LENGTH,
			"Application Server Host has invalid length."),
	MESSAGE_INVALID_LENGTH(ErrorConstants.MESSAGE_INVALID_LENGTH, "Message has invalid length.");
	
	private final String code;
	private final String message;

	/**
	 * @param code String
	 * @param message String
	 */
	ErrorRegistry(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	private static final Map<String, ErrorRegistry> LOOKUP = new HashMap<String, ErrorRegistry>();

	static {
		for (ErrorRegistry s : EnumSet.allOf(ErrorRegistry.class)) {
			LOOKUP.put(s.getCode(), s);
		}
	}

	/**
	 * @param code - String
	 * @return ErrorRegistry
	 */
	public static ErrorRegistry get(final String code) {
		return LOOKUP.get(code);
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return code + " : " + message;
	}

	/**
	 * @author dherzog
	 * 
	 * Error constant codes
	 *
	 */
	public class ErrorConstants {
		public static final String GENERAL_SERVICE_EXCEPTION = "00";
		public static final String SERVICE_AUTHENTICATION_FAILED = "01";
		public static final String SERVICE_REQUEST_IS_NULL = "02";
		public static final String AUDIT_CANNOT_BE_NULL = "03";
		public static final String AUDIT_CLOB_CANNOT_BE_NULL = "04";
		public static final String REQUESTOR_INVALID_LENGTH = "05";
		public static final String APPLICATION_INVALID_LENGTH = "06";
		public static final String SERVICE_INVALID_LENGTH = "07";
		public static final String CATEGORY_INVALID_LENGTH = "08";
		public static final String SUB_CATEGORY_INVALID_LENGTH = "09";
		public static final String APP_SERVER_IP_INVALID_LENGTH = "10";
		public static final String APP_SERVER_HOST_INVALID_LENGTH = "11";
		public static final String MESSAGE_INVALID_LENGTH = "12";
	}

}
