package com.ljhsquared.audit.service;

import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.dtos.AuditClob;


/**
 * @author dherzog
 * 
 * Audit Service Interface
 */
public interface AuditService {
	
	/**
	 * Create audit record.
	 * 
	 * @param audit - Audit
	 * @param auditClob - AuditClob
	 * @throws Exception - exception to throw
	 */
	public void create(final Audit audit, final AuditClob auditClob) throws Exception;
	
	
	/**
	 * Purge Clob data
	 * 
	 * @throws Exception - exception to throw
	 */
	public void purge() throws Exception;

    /**
     * Email if any audit records where created for the previous day
     *
     * @throws Exception - exception to throw
     */
    public void email() throws Exception;

}
