package com.ljhsquared.audit.service;

import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;
import com.ljhsquared.audit.business.mapper.AuditClobMapper;
import com.ljhsquared.audit.business.mapper.AuditMapper;
import com.ljhsquared.audit.dao.AuditClobDAO;
import com.ljhsquared.audit.dao.AuditDAO;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.entities.EAudit;
import com.ljhsquared.audit.entities.EAuditClob;

/**
 * @author dherzog
 * 
 * Audit Service Implementation
 */
@Service("auditService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuditServiceImpl implements AuditService {

	@Value("${audit.purge.days}")
	private int purgeDays;

    @Value("${audit.purge.process}")
    private boolean purgeProcess;

    @Value("${audit.email.days}")
    private int emailDays;

    @Value("${audit.email.process}")
    private boolean emailProcess;
    
    @Value("${audit.email.from}")
    private String emailFrom;
    
    @Value("${audit.email.to}")
    private String emailTo;

    @Autowired
	private AuditMapper auditMapper;
	@Autowired
	private AuditClobMapper auditClobMapper;
	@Autowired
	private AuditDAO auditDAO;
	@Autowired
	private AuditClobDAO auditClobDAO;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private VelocityEngine velocityEngine;
	
	@Override
	@Async
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void create(final Audit auditDTO, final AuditClob auditClobDTO) throws Exception {
		
		EAudit audit = auditMapper.mapFromDto(auditDTO);
		EAuditClob auditClob = auditClobMapper.mapFromDto(auditClobDTO);
		
		auditDAO.save(audit);
		auditClob.setAuditId(audit.getId());
		auditClobDAO.save(auditClob);
	}

	@Override
	@Scheduled(cron = "${audit.purge.clob}")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void purge() throws Exception {

        // check to see if email process is turned on for this server
        if (!purgeProcess) {
            return;
        }

		auditClobDAO.purge(-purgeDays);
	}

    @Override
    @Scheduled(cron = "${audit.email.count}")
    public void email() throws Exception {

        // check to see if email process is turned on for this server
        if (!emailProcess) {
            return;
        }

        // get count, do not send email if zero records found
        int count = auditDAO.count(emailDays);
        if (count <= 0) {
            return;
        }

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(emailFrom);
        helper.setTo(emailTo);
        helper.setSubject("Audit issues");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("count", String.valueOf(count));
        String emailText = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "emailTemplate.vm", model);
        helper.setText(emailText, true);

        mailSender.send(message);
    }
}
