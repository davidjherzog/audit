package com.ljhsquared.audit.service;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.ljhsquared.healthcheck.dtos.ConnectionCheckDTO;
import com.ljhsquared.healthcheck.dtos.NameValuePairDTO;
import com.ljhsquared.healthcheck.dtos.TableAccessCheckDTO;
import com.ljhsquared.healthcheck.service.AbstractHealthCheckService;
import com.ljhsquared.healthcheck.service.HealthCheckService;

/**
 * @author dherzog
 * 
 * Health Check Service Implementation
 * 
 */
@Service("healthCheckService")
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public class HealthCheckServiceImpl extends AbstractHealthCheckService implements HealthCheckService {

	public static final String AUDIT = "audit";
	
	@PersistenceContext(unitName = "audit")
	private EntityManager entityManager;

	@Override
	public List<ConnectionCheckDTO> checkDataBaseConnections() throws Exception {

		final List<ConnectionCheckDTO> checkDTOs = new ArrayList<ConnectionCheckDTO>();
		checkDTOs.add(checkConnectionFor(entityManager, AUDIT));
		return checkDTOs;
	}

	@Override
	public List<TableAccessCheckDTO> checkTableAccess() throws Exception {

		final List<TableAccessCheckDTO> checkDTOs = new ArrayList<TableAccessCheckDTO>();
		checkDTOs.add(checkTableAccessFor(entityManager, AUDIT, "APP_AUDIT"));
		checkDTOs.add(checkTableAccessFor(entityManager, AUDIT, "APP_AUDIT_AUX"));
		return checkDTOs;
	}

	@Override
	public List<NameValuePairDTO> getBuildInfoFromManifest(final ServletContext servletContext) throws Exception {
		return constructBuildInfoFromManifest(servletContext);
	}

}
