package com.ljhsquared.audit.aspect;

import com.ljhsquared.audit.dtos.MyRequest;
import com.ljhsquared.audit.dtos.MyResponse;
import com.ljhsquared.audit.errorhandling.ApplicationError;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author dherzog
 *
 * Facade Aspect Test
 */
@ContextConfiguration(locations = "/applicationAspect-test.xml")
public class ControllerAspectTest extends AbstractTestNGSpringContextTests {

	private ProceedingJoinPoint joinPoint;
	private Signature signature;
	private static final String SIGNATURE = "private final com.ljhsquared.audit.dtos.MyResponse";
	
	/**
	 * Setup up test.
	 */
	@BeforeMethod
	protected void init() {
		joinPoint = mock(ProceedingJoinPoint.class);
		signature = mock(Signature.class);
	}

	/**
	 * Test that should succeed.
	 * @throws Throwable - exception to be thrown
	 */
	@Test
	public void testShouldSucceed() throws Throwable {
		
		Object[] objects = { new MyRequest() };
		MyResponse responseDTO = new MyResponse();
		when(joinPoint.getArgs()).thenReturn(objects);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(joinPoint.getSignature().toLongString()).thenReturn(SIGNATURE);
		when(joinPoint.proceed()).thenReturn(responseDTO);
		
		MyResponse response = (MyResponse) new ControllerAspect().logAround(joinPoint);
        Assert.assertNotNull(response);
		
	}
	
	/**
	 * Test that should fail because of null request.
	 * 
	 * @throws Throwable - exception to be thrown
	 */
	@Test
	public void testRequestValidationShouldFailWhenRequestIsNull() throws Throwable {
		MyRequest requestDTO = null;
		Object[] objects = { requestDTO };
		when(joinPoint.getArgs()).thenReturn(objects);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(joinPoint.getSignature().toLongString()).thenReturn(SIGNATURE);

		try {
            MyResponse responseDTO = (MyResponse) new ControllerAspect().logAround(joinPoint);
            Assert.fail("null exception should have been thrown ");
        } catch (ApplicationException e) {
            Assert.assertEquals(e.getError().getCode(), ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(),
                    "null request DTO should result in a specific error code");
        }
	}
	
	/**
	 * Test that should handle ApplicationException.
	 * 
	 * @throws Throwable - exception to be thrown
	 */
	@Test
	public void testShouldHandleApplicationException() throws Throwable {
		Object[] objects = { new MyRequest() };
		when(joinPoint.getArgs()).thenReturn(objects);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(joinPoint.getSignature().toLongString()).thenReturn(SIGNATURE);
		when(joinPoint.proceed()).thenThrow(
				new ApplicationException(new ApplicationError(ErrorRegistry.GENERAL_SERVICE_EXCEPTION)));

		try {
            MyResponse responseDTO = (MyResponse) new ControllerAspect().logAround(joinPoint);
            Assert.fail("ApplicationException should have been thrown ");
        } catch (ApplicationException e) {
            Assert.assertEquals(e.getError().getCode(), ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getCode(),
                    "request DTO should result in a specific application error code");
        }
	}
	
	/**
	 * Test that handle a RuntimeException.
	 * 
	 * @throws Throwable - exception to be thrown
	 */
	@Test
	public void testShouldHandleRuntimeException() throws Throwable {
		
		Object[] objects = { new MyRequest() };
		when(joinPoint.getArgs()).thenReturn(objects);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(joinPoint.getSignature().toLongString()).thenReturn(SIGNATURE);
		when(joinPoint.proceed()).thenThrow(new RuntimeException());

		try {
            MyResponse responseDTO = (MyResponse) new ControllerAspect().logAround(joinPoint);
            Assert.fail("RuntimeException should have been thrown ");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof RuntimeException,
                    "exception should be instance of RuntimeException");
        }

	}

}
