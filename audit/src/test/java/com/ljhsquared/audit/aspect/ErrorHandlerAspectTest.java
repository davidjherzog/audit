package com.ljhsquared.audit.aspect;

import com.ljhsquared.audit.dtos.base.ErrorResponse;
import com.ljhsquared.audit.errorhandling.ApplicationError;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * @author dherzog
 *
 * Error Handler Aspect Test
 */
@ContextConfiguration(locations = "/applicationAspect-test.xml")
public class ErrorHandlerAspectTest extends AbstractTestNGSpringContextTests {

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private MethodParameter parameter;

    @Mock
    private BindingResult bindingResult;
    
    @Mock
    private FieldError fieldError;
    
    @Autowired
    private ErrorHandlerAspect aspect;

    /**
     * Setup up test.
     */
    @BeforeMethod
    protected void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test Application Error
     *
     * @throws Exception - exception to be thrown
     */
    @Test
    public void testApplicationError() throws Exception {

    	ErrorResponse response = aspect.applicationError(servletRequest,
                new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL)));
        Assert.assertEquals(response.getError().getCode(), ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(),
                "Error code is invalid.");
        Assert.assertEquals(response.getError().getMessage(), ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(),
                "Error message is invalid.");
    }

    /**
     * Test Validation Error
     *
     * @throws Exception - exception to be thrown
     */
    @Test
    public void testValidationError() throws Exception {

        for (ErrorRegistry error : ErrorRegistry.values()) {
            List<ObjectError> objectErrors = new ArrayList<ObjectError>();
            objectErrors.add(fieldError);
            
            when(fieldError.getDefaultMessage()).thenReturn(error.getCode());
            when(bindingResult.getAllErrors()).thenReturn(objectErrors);
            
            MethodArgumentNotValidException ve = new MethodArgumentNotValidException(parameter, bindingResult);

            ErrorResponse response = aspect.validationError(servletRequest, ve);
            Assert.assertEquals(response.getError().getCode(), error.getCode(),
                    "Error code is invalid.");
            Assert.assertEquals(response.getError().getMessage(), error.getMessage(),
                    "Error message is invalid.");
        }

    }

    /**
     * Test SQL Error
     *
     * @throws Exception - exception to be thrown
     */
    @Test
    public void testSQLError() throws Exception {
        
		SQLException exception =
				new SQLException("ORA-20100: json_node exception: attempt to get a number from a node with type ()",
						"ORA-20100");

		ErrorResponse response = aspect.sqlError(servletRequest, exception);
        Assert.assertEquals(response.getError().getCode(), ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getCode(),
                "Error code is invalid.");
        Assert.assertEquals(response.getError().getMessage(), ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getMessage(),
                "Error message is invalid.");
    }

    /**
     * Test Server Error
     *
     * @throws Exception - exception to be thrown
     */
    @Test
    public void testServerError() throws Exception {

    	ErrorResponse response = aspect.serverError(servletRequest, new Exception());
        Assert.assertEquals(response.getError().getCode(), ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getCode(),
                "Error code is invalid.");
        Assert.assertEquals(response.getError().getMessage(), ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getMessage(),
                "Error message is invalid.");
    }

}
