package com.ljhsquared.audit.aspect;

import com.ljhsquared.audit.errorhandling.ApplicationError;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author dherzog
 *
 * Validation Aspect - only for testing
 */
public class ValidationAspect {
	
	private Validator validator;
	
	public Validator getValidator() {
		return validator;
	}

	public void setValidator(final Validator validator) {
		this.validator = validator;
	}

	/**
	 * Proxy method to test validation
	 * 
	 * @param joinPoint ProceedingJoinPoint
	 * @return object
	 * @throws Throwable throw exception
	 */
	public final Object validateRequest(final ProceedingJoinPoint joinPoint) throws Throwable {
		
		try {
			validate(joinPoint);
			Object result = joinPoint.proceed();
			return result;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void validate(final ProceedingJoinPoint joinPoint) throws Exception {
		
		// ConstraintViolations to return
	   Set<ConstraintViolation<?>> violations = new HashSet<ConstraintViolation<?>>();
	   
	   // Get the target method
		Method interfaceMethod = ((MethodSignature) joinPoint.getSignature())
				.getMethod();
		Method implementationMethod = joinPoint
				.getTarget()
				.getClass()
				.getMethod(interfaceMethod.getName(),
						interfaceMethod.getParameterTypes());

	   // Get the annotated parameters and validate those with the @Valid annotation
		Annotation[][] annotationParameters = implementationMethod
				.getParameterAnnotations();
		for (int i = 0; i < annotationParameters.length; i++) {
			Annotation[] annotations = annotationParameters[i];
			for (Annotation annotation : annotations) {
				if (annotation.annotationType().equals(Valid.class)) {
					Object arg = joinPoint.getArgs()[i];
					violations
							.addAll(validator.validate(arg));
				}
			}
		}
	         
		// Throw an exception if ConstraintViolations are found
		if (!violations.isEmpty()) {
			Iterator<ConstraintViolation<?>> iter = violations.iterator();
		    while (iter.hasNext()) {
		    	ConstraintViolation<?> violation = iter.next();
		    	ApplicationError error = new ApplicationError(ErrorRegistry.get(violation.getMessage()));
		    	throw new ApplicationException(error);
		    }
		}
	}

}
