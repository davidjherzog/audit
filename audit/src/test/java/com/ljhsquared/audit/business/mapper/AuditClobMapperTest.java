package com.ljhsquared.audit.business.mapper;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.entities.EAuditClob;

/**
 * @author dherzog
 * 
 * Audit Clob Mapper Test
 */
public class AuditClobMapperTest {
	
	private static final int GUID_LENGTH = 36;
	private static final String REQUEST = "{request}";
	private static final String RESPONSE = "{response}";
	private static final String EXCEPTION = "exception stacktrace";

	/**
	 * Test mapping audit clob dto to audit clob.
	 * 
	 * @throws Exception - exception to throw
	 */
	@Test
	public void testMapFromDto() throws Exception {
		
		AuditClob auditClobDTO = new AuditClob();
		//auditClobDTO.setId(ID);
		auditClobDTO.setRequest(REQUEST);
		auditClobDTO.setResponse(RESPONSE);
		auditClobDTO.setException(EXCEPTION);
		
		AuditClobMapperImpl auditClobMapper = new AuditClobMapperImpl();
		EAuditClob auditClob = auditClobMapper.mapFromDto(auditClobDTO);
		
		Assert.assertEquals(auditClob.getId().length(), GUID_LENGTH, "ID is invalid.");
		Assert.assertEquals(auditClob.getRequest(), REQUEST, "Request is invalid.");
		Assert.assertEquals(auditClob.getResponse(), RESPONSE, "Response is invalid.");
		Assert.assertEquals(auditClob.getException(), EXCEPTION, "Exception is invalid.");
	}
	
}
