package com.ljhsquared.audit.business.mapper;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.entities.EAudit;

/**
 * @author dherzog
 * 
 * Audit Mapper Test
 */
public class AuditMapperTest {
	
	private static final String REQUESTOR = "1234567890";
	private static final String APPLICATION = "hostedpaymentpage";
	private static final String SERVICE = "/some/api/service";
	private static final String CATEGORY = "merchant";
	private static final String SUB_CATEGORY = "create";
	private static final String APP_SERVER_IP = "1.1.1.1";
	private static final String APP_SERVER_HOST = "server1";
	private static final String MESSAGE = "Test message";
	
	/**
	 * Test mapping audit dto to audit.
	 * 
	 * @throws Exception - exception to throw
	 */
	@Test
	public void testMapFromDto() throws Exception {
		
		Audit auditDTO = new Audit();
		auditDTO.setRequestor(REQUESTOR);
		auditDTO.setApplication(APPLICATION);
		auditDTO.setService(SERVICE);
		auditDTO.setCategory(CATEGORY);
		auditDTO.setSubCategory(SUB_CATEGORY);
		auditDTO.setAppServerIP(APP_SERVER_IP);
		auditDTO.setAppServerHost(APP_SERVER_HOST);
		auditDTO.setMessage(MESSAGE);
		
		AuditMapperImpl auditMapper = new AuditMapperImpl();
		EAudit audit = auditMapper.mapFromDto(auditDTO);
		
		Assert.assertEquals(audit.getRequestor(), REQUESTOR, "Requestor is invalid.");
		Assert.assertEquals(audit.getApplication(), APPLICATION, "Application is invalid.");
		Assert.assertEquals(audit.getService(), SERVICE, "Service is invalid.");
		Assert.assertEquals(audit.getCategory(), CATEGORY, "Category is invalid.");
		Assert.assertEquals(audit.getSubCategory(), SUB_CATEGORY, "Sub-Category is invalid.");
		Assert.assertEquals(audit.getAppServerIP(), APP_SERVER_IP, "Application Server IP is invalid.");
		Assert.assertEquals(audit.getAppServerHost(), APP_SERVER_HOST, "Application Server Host is invalid.");
		Assert.assertEquals(audit.getMessage(), MESSAGE, "Message is invalid.");
	}

}
