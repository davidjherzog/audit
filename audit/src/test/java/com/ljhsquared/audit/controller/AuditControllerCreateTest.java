package com.ljhsquared.audit.controller;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.dtos.AuditCreateRequest;
import com.ljhsquared.audit.dtos.AuditCreateResponse;
import com.ljhsquared.audit.errorhandling.ApplicationError;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import com.ljhsquared.audit.service.AuditService;


/**
 * @author dherzog
 *
 * Audit Controller Create Test
 */
@ContextConfiguration(locations = "/applicationValidator-test.xml")
public class AuditControllerCreateTest extends AbstractTestNGSpringContextTests {

	@Mock
	private AuditService auditService;
	@InjectMocks
	private AuditControllerImpl auditController;
	private Audit expectedAuditDTO;
	private AuditClob expectedAuditClobDTO;
	
	private static final String AUTH = "Basic dGVzdGluZzp0ZXN0aW5nMTIz";
	
	/**
	 * Setup up test.
	 */
	@BeforeMethod
	public void init() {
		auditController = new AuditControllerImpl();
		expectedAuditDTO = new Audit();
		expectedAuditDTO.setRequestor("1234567890");
		expectedAuditDTO.setApplication("hostedpaymentpage");
		expectedAuditDTO.setService("/some/api/service");
		expectedAuditDTO.setCategory("merchant");
		expectedAuditDTO.setSubCategory("create");
		expectedAuditDTO.setAppServerIP("1.1.1.1");
		expectedAuditDTO.setAppServerHost("server1");
		expectedAuditDTO.setMessage("test message");
		expectedAuditClobDTO = new AuditClob();
		expectedAuditClobDTO.setRequest("Request");
		expectedAuditClobDTO.setResponse("Response");
		expectedAuditClobDTO.setException("Exception");
		expectedAuditDTO.setClob(expectedAuditClobDTO);
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test creating an audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public final void shouldReturnSuccessfulCreate() throws Exception {

		AuditCreateRequest request = new AuditCreateRequest();
		
		request.setAudit(expectedAuditDTO);
		
		doAnswer(new Answer<Void>() {
	        @Override
	        public Void answer(final InvocationOnMock invocation) throws Throwable {

	            return null;
	        }
	    }).when(auditService).create(expectedAuditDTO, expectedAuditDTO.getClob());
	    
		// TODO fix this test
		AuditCreateResponse responseDTO = auditController.create(request, AUTH);

		//Assert.assertNull(responseDTO.getError(), "Audit create error response should be null.");

	}
	
	/**
	 * Test error returned in create response.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(enabled = false)
	public final void shouldThrowAuthenticationError() throws Exception {

		AuditCreateRequest request = new AuditCreateRequest();
		request.setAudit(expectedAuditDTO);
		
		AuditCreateResponse responseDTO =
				auditController.create(request, null);

		/*Assert.assertNotNull(responseDTO.getError(), "Audit create response error is null.");
		Assert.assertEquals(responseDTO.getError().getCode(),
				ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getCode(), "Exception code invalid.");
		Assert.assertEquals(responseDTO.getError().getMessage(),
				ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getMessage(), "Exception message invalid.");*/

	}
	
	/**
	 * Test error returned in create response.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(enabled = false)
	public final void shouldReturnErrorInCreateDto() throws Exception {

		AuditCreateRequest request = new AuditCreateRequest();
		request.setAudit(expectedAuditDTO);
		
		doThrow(new ApplicationException(new ApplicationError(ErrorRegistry.GENERAL_SERVICE_EXCEPTION))).when(
				auditService).create(expectedAuditDTO, expectedAuditClobDTO);
		
		AuditCreateResponse responseDTO =
				auditController.create(request, AUTH);

		/*Assert.assertNotNull(responseDTO.getError(), "Audit create response error is null.");
		Assert.assertEquals(responseDTO.getError().getCode(),
				ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getCode(), "Exception code invalid.");
		Assert.assertEquals(responseDTO.getError().getMessage(),
				ErrorRegistry.GENERAL_SERVICE_EXCEPTION.getMessage(), "Exception message invalid.");*/

	}
	
	/**
	 * Test error returned in create request is null.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(enabled = false)
	public final void shouldReturnErrorBecauseOfNullRequest() throws Exception {

		AuditCreateResponse responseDTO = auditController.create(null, null);

		/*Assert.assertNotNull(responseDTO.getError(), "Audit create response error is not null.");
		Assert.assertEquals(responseDTO.getError().getCode(),
				ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(), "Exception code invalid.");
		Assert.assertEquals(responseDTO.getError().getMessage(),
				ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");*/

	}
}
