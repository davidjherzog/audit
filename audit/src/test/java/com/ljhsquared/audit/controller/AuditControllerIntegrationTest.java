package com.ljhsquared.audit.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ljhsquared.audit.controller.base.BaseInMemoryDBTransactionalTest;
import com.ljhsquared.audit.dao.AuditClobDAO;
import com.ljhsquared.audit.dao.AuditDAO;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.dtos.AuditCreateRequest;
import com.ljhsquared.audit.dtos.AuditCreateResponse;
import com.ljhsquared.audit.entities.EAudit;
import com.ljhsquared.audit.entities.EAuditClob;
import com.ljhsquared.audit.errorhandling.ApplicationException;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;


/**
 * @author dherzog
 *
 * Audit Controller Integration Test
 */
public class AuditControllerIntegrationTest extends BaseInMemoryDBTransactionalTest {
	
	private static final String REQUESTOR = "1234567890";
	private static final String APPLICATION = "hostedpaymentpage";
	private static final String SERVICE = "/some/api/service";
	private static final String CATEGORY = "merchant";
	private static final String SUB_CATEGORY = "create";
	private static final String APP_SERVER_IP = "1.1.1.1";
	private static final String APP_SERVER_HOST = "server1";
	private static final String MESSAGE = "Test message";
	private static final String REQUEST = "{request}";
	private static final String RESPONSE = "{response}";
	private static final String EXCEPTION = "exception stacktrace";
	private static final int GUID_LENGTH = 36;
	private static final String AUTH = "Basic dGVzdGluZzp0ZXN0aW5nMTIz";
	
	@Autowired
	private AuditController auditController;
	
	@Autowired
	private AuditDAO auditDAO;
	
	@Autowired 
	private AuditClobDAO auditClobDAO;
	
	/**
	 * Initialize data for test class.
	 */
	@BeforeClass
	public void initClass() {
		executeSqlScript("classpath:/audit.sql", true);
		//org.hsqldb.util.DatabaseManagerSwing.main(new String[] {
		//  "--url",  "jdbc:hsqldb:mem:audit", "--noexit"
		//});
		
	}
	
	/**
	 * Test error creating audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(groups = "testCreateError")
	public final void testCreateError() throws Exception {

		// create audit
		AuditCreateRequest createRequest = new AuditCreateRequest();
		
		try {
			auditController.create(createRequest, AUTH);
            Assert.fail("Audit create should have failed due to invalid request");
        } catch (ApplicationException ae) {
        	Assert.assertNotNull(ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getCode(), "Exception code invalid.");
        	Assert.assertNotNull(ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getMessage(), 
        			"Exception message invalid.");
        }
	}
	
	/**
	 * Test authentication error creating audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(groups = "testCreateAuthenticationError", dependsOnGroups = "testCreateError")
	public final void testCreateAuthenticationError() throws Exception {

		// create audit
		AuditCreateRequest createRequest = new AuditCreateRequest();
		createRequest.setAudit(createAuditDto());
		
		try {
			auditController.create(createRequest, null);
            Assert.fail("Audit create should have failed due to authentication error");
        } catch (ApplicationException ae) {
        	Assert.assertEquals(ae.getError().getCode(),
                    ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getCode(), "Exception code invalid.");
        	Assert.assertEquals(ae.getError().getMessage(),
                    ErrorRegistry.SERVICE_AUTHENTICATION_FAILED.getMessage(), "Exception message invalid.");
        }
		
	}
	
	/**
	 * Test creating audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(groups = "testCreate", dependsOnGroups = "testCreateAuthenticationError")
	public final void testCreate() throws Exception {

		// create audit
		AuditCreateRequest createRequest = new AuditCreateRequest();
		createRequest.setAudit(createAuditDto());
		AuditCreateResponse createResponse = auditController.create(createRequest, AUTH);

		Assert.assertNotNull(createResponse, "Audit create response should not be null.");

	}
	
	/**
	 * Test finding audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test(groups = "testFindCreate", dependsOnGroups = "testCreate")
	public final void testFindCreate() throws Exception {

		List<EAudit> audits = auditDAO.findAll();
		EAudit audit = audits.get(0);
		List<EAuditClob> auditClobs = auditClobDAO.findAll();
		EAuditClob auditClob = auditClobs.get(0);
		
		Assert.assertEquals(audit.getId().length(), GUID_LENGTH, "ID is invalid.");
		Assert.assertEquals(audit.getRequestor(), REQUESTOR, "Requestor is invalid.");
		Assert.assertEquals(audit.getApplication(), APPLICATION, "Application is invalid.");
		Assert.assertEquals(audit.getService(), SERVICE, "Service is invalid.");
		Assert.assertEquals(audit.getCategory(), CATEGORY, "Category is invalid.");
		Assert.assertEquals(audit.getSubCategory(), SUB_CATEGORY, "Sub-Category is invalid.");
		Assert.assertEquals(audit.getAppServerIP(), APP_SERVER_IP, "Application Servier IP is invalid.");
		Assert.assertEquals(audit.getAppServerHost(), APP_SERVER_HOST, "Application Servier Host is invalid.");
		Assert.assertEquals(audit.getMessage(), MESSAGE, "Message is invalid.");
		Assert.assertNotNull(audit.getCreatedDate(), "Create Date is null.");
		
		Assert.assertEquals(auditClob.getId().length(), GUID_LENGTH, "ID is invalid.");
		Assert.assertEquals(auditClob.getAuditId().length(), GUID_LENGTH, "Audit ID is invalid.");
		Assert.assertEquals(auditClob.getRequest(), REQUEST, "Request is invalid.");
		Assert.assertEquals(auditClob.getResponse(), RESPONSE, "Response is invalid.");
		Assert.assertEquals(auditClob.getException(), EXCEPTION, "Exception is invalid.");
		Assert.assertNotNull(auditClob.getCreatedDate(), "Create Date is null.");
		
		Assert.assertNotEquals(audit.getId(), auditClob.getId(), "Audit Id and Audit Clob ID should not be the same.");
		Assert.assertEquals(audit.getId(), auditClob.getAuditId(), "Id and Audit ID in Clob should be the same.");
		System.out.println(audit.getId());

	}

    /**
     * Test audit count.
     *
     * @throws Exception - exception to be thrown
     */
    @Test(groups = "testCount", dependsOnGroups = "testFindCreate")
    public final void testCount() throws Exception {

        int count = auditDAO.count(1);
        Assert.assertEquals(count, 1, "Count is invalid.");

    }
    
    private Audit createAuditDto() {
    	Audit audit = new Audit();
		audit.setRequestor(REQUESTOR);
		audit.setApplication(APPLICATION);
		audit.setService(SERVICE);
		audit.setCategory(CATEGORY);
		audit.setSubCategory(SUB_CATEGORY);
		audit.setAppServerIP(APP_SERVER_IP);
		audit.setAppServerHost(APP_SERVER_HOST);
		audit.setMessage(MESSAGE);
		
		AuditClob auditClob = new AuditClob();
		auditClob.setRequest(REQUEST);
		auditClob.setResponse(RESPONSE);
		auditClob.setException(EXCEPTION);
		
		audit.setClob(auditClob);
		
		return audit;
    }

}
