package com.ljhsquared.audit.controller.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Base class for in-memory DB tests.
 * 
 * @author dherzog
 *
 */

@ContextConfiguration(locations = "/applicationDB-test.xml")
@Transactional
@TransactionConfiguration(defaultRollback = false)
public abstract class BaseInMemoryDBTransactionalTest extends AbstractTransactionalTestNGSpringContextTests {

	@Autowired
	@Override
	public void setDataSource(@Qualifier("hsqlMemoryDb") final javax.sql.DataSource dataSource) {
		super.setDataSource(dataSource);
	};
	
}
