package com.ljhsquared.audit.dtos;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author dherzog
 *
 * @param <R>
 */
@ContextConfiguration(locations = "/applicationValidator-test.xml")
public abstract class AbstractDTOTests<R> extends AbstractTestNGSpringContextTests {
	
	@Autowired
    private Validator validator;
	protected R request;
	private Set<ConstraintViolation<R>> violations;
	
	/**
	 * Test successful request.
	 */
	@Test
	public final void testSuccess() {
		assertTrue();
	}
	
	/**
	 * Check if request is valid.
	 */
	protected void assertTrue() {
		validate();
		Assert.assertTrue(violations.isEmpty(), "Successful request failed");
	}
	
	/**
	 * @param message - Display message if assertion fails
	 */
	protected void assertFalse(final String message) {
		validate();
		Assert.assertFalse(violations.isEmpty(), message);
	}
	
	/**
	 * @param error - Error code
	 * @param message - Display message if assertion fails
	 */
	protected void assertErrorMessage(final String error, final String message) {
		
		validate();
		Assert.assertFalse(violations.isEmpty(), message);
		
		Iterator<ConstraintViolation<R>> iter = violations.iterator();
		while (iter.hasNext()) {
			ConstraintViolation<R> violation = iter.next();
			Assert.assertEquals(violation.getMessage(), error, message);
			break;
		}
	}
	
	/**
	 * Used to create a parameter that exceeds the max length of the parameter being tested.
	 *  
	 * @param maxLength - Max length of parameter
	 * @return String
	 */
	protected String createStringToExcedeMaxLength(final int maxLength) {
		StringBuffer sb = new StringBuffer(maxLength + 1);
		for (int i = 0; i < maxLength + 1; i++) {
			sb.append("a");
		}
		return sb.toString();
	}

	private void validate() {
		violations = validator.validate(request);
	}

}
