package com.ljhsquared.audit.dtos;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ljhsquared.audit.business.validator.ValidConstants;
import com.ljhsquared.audit.errorhandling.ErrorRegistry;
import com.ljhsquared.audit.errorhandling.ErrorRegistry.ErrorConstants;


/**
 * @author dherzog
 * 
 * Abstract Audit Create Request DTO test
 */
public class AuditCreateRequestTest extends AbstractDTOTests<AuditCreateRequest> {
	
	private Audit audit;
	
	/**
	 * Initialize Request.
	 */
	@BeforeMethod
	public void init() {
		audit = new Audit();
		audit.setRequestor("1234567890");
		audit.setService("/some/api/service");
		audit.setApplication("hostedpaymentpage");
		audit.setCategory("merchant");
		audit.setSubCategory("create");
		audit.setAppServerIP("1.1.1.1");
		audit.setAppServerHost("server1");
		audit.setMessage("Test message");
		
		AuditClob auditClob = new AuditClob();
		audit.setClob(auditClob);
		
		request = new AuditCreateRequest();
		request.setAudit(audit);
	}
	
	/**
	 * Test error when Audit Create request is null.
	 */
	@Test
	public final void testAuditNull() {
		
		request.setAudit(null);
		assertErrorMessage(ErrorConstants.AUDIT_CANNOT_BE_NULL,
				ErrorRegistry.AUDIT_CANNOT_BE_NULL.getMessage());
		
	}
	
	/**
	 * Test error when requestor is to big or small or null.
	 */
	@Test
	public final void testRequestorSize() {
		
		// test min
		audit.setRequestor("");
		assertTrue();
		
		// test max
		audit.setRequestor(createStringToExcedeMaxLength(ValidConstants.REQUESTOR_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.REQUESTOR_INVALID_LENGTH,
				ErrorRegistry.REQUESTOR_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setRequestor(null);
		assertErrorMessage(ErrorConstants.REQUESTOR_INVALID_LENGTH,
				ErrorRegistry.REQUESTOR_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when service is to big or small or null.
	 */
	@Test
	public final void testServiceSize() {
		
		// test min
		audit.setService("");
		assertTrue();
		
		// test max
		audit.setService(createStringToExcedeMaxLength(ValidConstants.SERVICE_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.SERVICE_INVALID_LENGTH,
				ErrorRegistry.SERVICE_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setService(null);
		assertErrorMessage(ErrorConstants.SERVICE_INVALID_LENGTH,
				ErrorRegistry.SERVICE_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when application is to big or small or null.
	 */
	@Test
	public final void testApplicationSize() {
		
		// test min
		audit.setApplication("");
		assertTrue();
		
		// test max
		audit.setApplication(createStringToExcedeMaxLength(ValidConstants.APPLICATION_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.APPLICATION_INVALID_LENGTH,
				ErrorRegistry.APPLICATION_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setApplication(null);
		assertErrorMessage(ErrorConstants.APPLICATION_INVALID_LENGTH,
				ErrorRegistry.APPLICATION_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when category is to big or small or null.
	 */
	@Test
	public final void testCategorySize() {
		
		// test min
		audit.setCategory("");
		assertTrue();
		
		// test max
		audit.setCategory(createStringToExcedeMaxLength(ValidConstants.CATEGORY_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.CATEGORY_INVALID_LENGTH,
				ErrorRegistry.CATEGORY_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setCategory(null);
		assertErrorMessage(ErrorConstants.CATEGORY_INVALID_LENGTH,
				ErrorRegistry.CATEGORY_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when sub-category is to big or small or null.
	 */
	@Test
	public final void testSubCategorySize() {
		
		// test min
		audit.setSubCategory("");
		assertTrue();
		
		// test max
		audit.setSubCategory(createStringToExcedeMaxLength(ValidConstants.SUB_CATEGORY_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.SUB_CATEGORY_INVALID_LENGTH,
				ErrorRegistry.SUB_CATEGORY_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setSubCategory(null);
		assertErrorMessage(ErrorConstants.SUB_CATEGORY_INVALID_LENGTH,
				ErrorRegistry.SUB_CATEGORY_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when application server IP is to big or small or null.
	 */
	@Test
	public final void testAppServerIPSize() {
		
		// test min
		audit.setAppServerIP("");
		assertTrue();
		
		// test max
		audit.setAppServerIP(createStringToExcedeMaxLength(ValidConstants.APP_SERVER_IP_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.APP_SERVER_IP_INVALID_LENGTH,
				ErrorRegistry.APP_SERVER_IP_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setAppServerIP(null);
		assertErrorMessage(ErrorConstants.APP_SERVER_IP_INVALID_LENGTH,
				ErrorRegistry.APP_SERVER_IP_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when application server Host is to big or small or null.
	 */
	@Test
	public final void testAppServerHostSize() {
		
		// test min
		audit.setAppServerHost("");
		assertTrue();
		
		// test max
		audit.setAppServerHost(createStringToExcedeMaxLength(ValidConstants.APP_SERVER_HOST_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.APP_SERVER_HOST_INVALID_LENGTH,
				ErrorRegistry.APP_SERVER_HOST_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setAppServerHost(null);
		assertErrorMessage(ErrorConstants.APP_SERVER_HOST_INVALID_LENGTH,
				ErrorRegistry.APP_SERVER_HOST_INVALID_LENGTH.getMessage());
		
	}
	
	/**
	 * Test error when message is to big or small or null.
	 */
	@Test
	public final void testMessageSize() {
		
		// test min
		audit.setMessage("");
		assertTrue();
		
		// test max
		audit.setMessage(createStringToExcedeMaxLength(ValidConstants.MESSAGE_MAX_LENGTH));
		assertErrorMessage(ErrorConstants.MESSAGE_INVALID_LENGTH,
				ErrorRegistry.MESSAGE_INVALID_LENGTH.getMessage());
		
		// check null value
		audit.setMessage(null);
		assertErrorMessage(ErrorConstants.MESSAGE_INVALID_LENGTH,
				ErrorRegistry.MESSAGE_INVALID_LENGTH.getMessage());
		
	}

}
