package com.ljhsquared.audit.dtos;

import com.ljhsquared.audit.dtos.base.Request;

/**
 * @author dherzog
 *
 * My Request DTO for testing FacadeAspect
 * 
 */
public class MyRequest extends Request {

	private static final long serialVersionUID = 1L;

}