package com.ljhsquared.audit.dtos;

import com.ljhsquared.audit.dtos.base.Response;

/**
 * @author dherzog
 *
 * My Response DTO for testing FacadeAspect
 */
public class MyResponse extends Response {

	private static final long serialVersionUID = 1L;

}
