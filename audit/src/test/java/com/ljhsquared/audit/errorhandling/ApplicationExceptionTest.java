package com.ljhsquared.audit.errorhandling;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test SP Application Exceptions.
 * 
 * @author herzod01
 *
 */
public class ApplicationExceptionTest {
	
	/**
	 * Test default constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testDefaultConstructor() throws Exception {
		
		try {
			throw new ApplicationException();
		} catch (ApplicationException sae) {
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test String, Throwable constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testStringThrowableConstructor() throws Exception {
		
		try {
			throw new ApplicationException(ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), new Throwable());
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test String constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testStringConstructor() throws Exception {
		
		try {
			throw new ApplicationException(ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage());
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test Throwable constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testThrowableConstructor() throws Exception {
		
		try {
			throw new ApplicationException(new Throwable(ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage()));
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getMessage(), "java.lang.Throwable: "
					+ ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(),
					"Exception message invalid.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test SpError constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testSpErrorConstructor() throws Exception {
		
		try {
			throw new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL));
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getError().getCode(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(), "Exception code invalid.");
			Assert.assertEquals(sae.getError().getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test SpError, SupppressStackTrace constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testSpErrorSupressStackTraceConstructor() throws Exception {
		
		try {
			throw new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL), true);
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getError().getCode(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(), "Exception code invalid.");
			Assert.assertEquals(sae.getError().getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
			Assert.assertTrue(sae.isSupressStackTrace(), "Exception stack trace should be true.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}
		
		try {
			throw new ApplicationException(new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL), false);
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getError().getCode(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(), "Exception code invalid.");
			Assert.assertEquals(sae.getError().getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
			Assert.assertFalse(sae.isSupressStackTrace(), "Exception stack trace should be true.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}
	
	/**
	 * Test SpError, Throwable constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testSpErrorThrowableConstructor() throws Exception {
		
		try {
			throw new ApplicationException(new ApplicationError(
					ErrorRegistry.SERVICE_REQUEST_IS_NULL), new Throwable());
		} catch (ApplicationException sae) {
			Assert.assertEquals(sae.getError().getCode(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getCode(), "Exception code invalid.");
			Assert.assertEquals(sae.getError().getMessage(),
					ErrorRegistry.SERVICE_REQUEST_IS_NULL.getMessage(), "Exception message invalid.");
		} catch (Exception e) {
			Assert.fail("SpApplicationException not thrown.");
		}

	}

}
