package com.ljhsquared.audit.errorhandling;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ljhsquared.audit.errorhandling.ErrorRegistry.ErrorConstants;

/**
 * Test SP Error Registry. 
 * 
 * @author herzod01
 *
 */
public class ErrorRegistryTest {
	
	/**
	 * Test synthetic method in enum to increase the package coverage.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testEnumSyntheticMethodForBetterPackageCovera() throws Exception {
		ErrorRegistry.valueOf(ErrorRegistry.SERVICE_REQUEST_IS_NULL.name());
	}
	
	/**
	 * Test error message in registry.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testShouldReturnTheRightErrorMessage() throws Exception {
		ErrorRegistry error = ErrorRegistry.SERVICE_REQUEST_IS_NULL;
		String message = error.getMessage();
		Assert.assertEquals(message, "The service request cannot be null.", "Error message is invalid.");
	}
	
	/**
	 * Test error code in registry.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testShouldReturnTheRightErrorCode() throws Exception {
		ErrorRegistry error = ErrorRegistry.SERVICE_REQUEST_IS_NULL;
		String code = error.getCode();
		Assert.assertEquals(code, ErrorConstants.SERVICE_REQUEST_IS_NULL, "Error code is invalid.");
	}
	
	/**
	 * Test toString in registry.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testShouldReturnTheRightToString() throws Exception {
		ErrorRegistry error = ErrorRegistry.SERVICE_REQUEST_IS_NULL;
		String code = error.toString();
		Assert.assertEquals(code, ErrorConstants.SERVICE_REQUEST_IS_NULL + " : The service request cannot be null.",
				"To string is invalid.");
	}

}
