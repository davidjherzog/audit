package com.ljhsquared.audit.errorhandling;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ljhsquared.audit.errorhandling.ErrorRegistry.ErrorConstants;

/**
 * Test SP Error.
 * 
 * @author herzod01
 *
 */
public class ErrorTest {
	
	private static final String MESSAGE = "Test Error";
	
	/**
	 * Test registry constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testRegistryConstructor() throws Exception {
		ApplicationError error = new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL);
		Assert.assertEquals(error.getCode(), ErrorConstants.SERVICE_REQUEST_IS_NULL, "Error code is invalid.");
		Assert.assertEquals(error.getMessage(), "The service request cannot be null.", "Error message is invalid.");
	}
	
	/**
	 * Test registry and message constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testRegistryMessageConstructor() throws Exception {
		ApplicationError error = new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL, MESSAGE);
		Assert.assertEquals(error.getCode(), ErrorConstants.SERVICE_REQUEST_IS_NULL, "Error code is invalid.");
		Assert.assertEquals(error.getMessage(), MESSAGE, "Error message is invalid.");
	}
	
	/**
	 * Test registry, message and append true constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testRegistryMessageAppendTrueConstructor() throws Exception {
		ApplicationError error = new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL, MESSAGE, true);
		Assert.assertEquals(error.getCode(), ErrorConstants.SERVICE_REQUEST_IS_NULL, "Error code is invalid.");
		Assert.assertEquals(error.getMessage(), "The service request cannot be null."
				+ " " + MESSAGE, "Error message is invalid.");
	}
	
	/**
	 * Test registry, message and append false constructor.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public void testRegistryMessageAppendFalseConstructor() throws Exception {
		ApplicationError error = new ApplicationError(ErrorRegistry.SERVICE_REQUEST_IS_NULL, MESSAGE, false);
		Assert.assertEquals(error.getCode(), ErrorConstants.SERVICE_REQUEST_IS_NULL, "Error code is invalid.");
		Assert.assertEquals(error.getMessage(), MESSAGE, "Error message is invalid.");
	}

}
