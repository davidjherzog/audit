package com.ljhsquared.audit.service;

import com.ljhsquared.audit.business.mapper.AuditClobMapper;
import com.ljhsquared.audit.business.mapper.AuditMapper;
import com.ljhsquared.audit.dao.AuditClobDAO;
import com.ljhsquared.audit.dao.AuditDAO;
import com.ljhsquared.audit.dtos.Audit;
import com.ljhsquared.audit.dtos.AuditClob;
import com.ljhsquared.audit.dtos.AuditCreateRequest;
import com.ljhsquared.audit.entities.EAudit;
import com.ljhsquared.audit.entities.EAuditClob;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;



/**
 * @author dherzog
 *
 * Audit Service Create Test
 */
public class AuditServiceCreateTest {
	
	@InjectMocks
	private AuditServiceImpl auditService;
	@Mock
	private AuditDAO auditDAO;
	@Mock
	private AuditClobDAO auditClobDAO;
	@Mock
	private AuditMapper auditMapper;
	@Mock
	private AuditClobMapper auditClobMapper;
	
	/**
	 * Setup up test.
	 */
	@BeforeMethod
	public void init() {
		auditService = new AuditServiceImpl();
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test creating a audit.
	 * 
	 * @throws Exception - exception to be thrown
	 */
	@Test
	public final void shouldReturnNoErrorsForCreate() throws Exception {
		
		AuditCreateRequest request = new AuditCreateRequest();
		EAudit expectedAudit = new EAudit();
		EAuditClob expectedAuditClob = new EAuditClob();
		Audit expectedAuditDTO = new Audit();
		AuditClob expectedAuditClobDTO = new AuditClob();
		request.setAudit(expectedAuditDTO);
		doAnswer(new Answer<Void>() {
	        @Override
	        public Void answer(final InvocationOnMock invocation) throws Throwable {

	            return null;
	        }
	    }).when(auditDAO).save(expectedAudit);
		doAnswer(new Answer<Void>() {
	        @Override
	        public Void answer(final InvocationOnMock invocation) throws Throwable {

	            return null;
	        }
	    }).when(auditClobDAO).save(expectedAuditClob);
		when(auditMapper.mapFromDto(expectedAuditDTO)).thenReturn(expectedAudit);
		when(auditClobMapper.mapFromDto(expectedAuditClobDTO)).thenReturn(expectedAuditClob);
		
		auditService.create(expectedAuditDTO, expectedAuditClobDTO);
	
	}

}
