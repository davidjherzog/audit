package com.ljhsquared.audit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dherzog
 * 
 * Auditor
 * 
 * This class hides all the details from the consumers about the Audit service so 
 * the client does not have to worry about the RESTful aspects of calling the Audit
 * service.
 *
 */
public class Auditor {
	
	private static Logger logger = LoggerFactory.getLogger(Auditor.class.getName());
	
	private static final String UNKNOWN = "unknown";
	
	private String url;
	private String auth;

	private String requestor;
	private String application;
	private String service;
	private String category;
	private String subCategory;
	private String message;
	private String request;
	private String response;
	private Exception exception;
	
	private String errorCode;
	private String errorMessage;
	
	/**
	 * @param url
	 * 
	 * Auditor constructor that takes the service URL.
	 */
	public Auditor(final String url) {
		this.url = url;
	}
	
	/**
	 * @param url - Audit URL
	 * @param auth - authentication 
	 * 
	 * Auditor constructor that takes the service URL.
	 */
	public Auditor(final String url, final String auth) {
		this.url = url;
		this.auth = auth;
	}

	/**
	 * After the Auditor object is loaded up with all the proper information, calling send will call the 
	 * audit/create RESTful service.
	 */
	public void send() {
		
		HttpClient httpClient = null;
		try {
			JSONObject audit =
					createAudit(InetAddress.getLocalHost().getHostAddress(), InetAddress.getLocalHost().getHostName());
			
			httpClient = new DefaultHttpClient();
			HttpPut putRequest = new HttpPut(url);
			putRequest.addHeader("Content-type", "application/json");
			putRequest.addHeader("Accept", "application/json");
			putRequest.addHeader("Authorization", "Basic " + auth);
			StringEntity sEntity = new StringEntity(audit.toJSONString());
			putRequest.setEntity(sEntity);
			
			HttpResponse httpResponse = httpClient.execute(putRequest);
			HttpEntity entity = httpResponse.getEntity();
			
			JSONObject jsonObject = (JSONObject) JSONValue.parse(read(entity.getContent()));
			
			errorCode = getParam(jsonObject, "error", "code");
			errorMessage = getParam(jsonObject, "error", "message");
		} catch (Exception e) {
			errorCode = "00";
			errorMessage = "General Service exception.";
			logger.error("Audit failed with the exception " + e + "\n");
		} finally {
			try {
				httpClient.getConnectionManager().shutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject createAudit(final String ip, final String host) {
		
		JSONObject auditCreate = new JSONObject();
		
		JSONObject audit = new JSONObject();
		audit.put("requestor", requestor);
		audit.put("application", application);
		audit.put("service", service);
		audit.put("category", category);
		audit.put("subCategory", subCategory);
		audit.put("appServerIP", ip);
		audit.put("appServerHost", host);
		audit.put("message", message);
		
		JSONObject clob = new JSONObject();
		clob.put("request", request);
		clob.put("response", response);
		
		if (exception != null) {
			StringWriter sw = new StringWriter();
			exception.printStackTrace(new PrintWriter(sw));
			clob.put("exception", sw.toString());
		} else {
			clob.put("exception", "");
		}
		
		audit.put("clob", clob);
		
		auditCreate.put("audit", audit);
		
		return auditCreate;
		
	}
	
	/**
	 * @param in - HttpEntity InputStream
	 * @return - InputStream as String
	 * @throws IOException - exception to be thrown
	 */
	private static String read(final InputStream in) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		int b;
		while ((b = in.read()) != -1) {
			out.write(b);
		}
		return out.toString();
	}
	
	/**
	 * @param j - JSON object
	 * @param s... - JSON attribute
	 * @return Value
	 */
	private String getParam(final JSONObject j, final String... s) {
		
		JSONObject temp = j;
		for (int i = 0; i < s.length - 1; i++) {
			temp = (JSONObject) j.get(s[i]);
	    }
		if (temp == null) {
			return null;
		}
		Object o = temp.get(s[s.length - 1]);
		if (o instanceof String) {
			return (String) o;
		}

		return null;
	}

	/**
	 * @param requestor the requestor to set
	 */
	public void setRequestor(final String requestor) {
		if ((requestor == null) || (requestor.trim().equals(""))) {
			this.requestor = UNKNOWN;
		} else {
			this.requestor = requestor;
		}
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(final String application) {
		this.application = application;
	}
	
	/**
	 * @param service the service to set
	 */
	public void setService(final String service) {
		this.service = service;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(final String category) {
		this.category = category;
	}

	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(final String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(final String request) {
		this.request = request;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(final String response) {
		this.response = response;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(final Exception exception) {
		this.exception = exception;
	}
	
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
}
