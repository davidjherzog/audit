package com.ljhsquared.audit;

import org.json.simple.JSONObject;


/**
 * @author dherzog
 * 
 * Test creating an Audit record.
 *
 */
public class AuditorTest {
	
	/**
	 * @param args - nothing to pass
	 */
	@SuppressWarnings("unchecked")
	public static void main(final String[] args) {
		
		JSONObject request = new JSONObject();
		request.put("value1", "abcdef");
		request.put("value2", "123456");
		
		JSONObject response = new JSONObject();
		response.put("code", "00");
		response.put("message", "General service exception.");
		
		Auditor auditor = new Auditor("https://pipeline.prinpay.com:8443/audit/create");
		auditor.setRequestor("0123456789");
		auditor.setApplication("hostedpaymentpage");
		auditor.setCategory("merchant");
		auditor.setSubCategory("create");
		auditor.setMessage("Error creating merchant.");
		auditor.setRequest(request.toJSONString());
		auditor.setResponse(response.toJSONString());
		auditor.setException(null);
		auditor.send();
		
		System.out.println("Error Code: " + auditor.getErrorCode());
		System.out.println("Error Message: " + auditor.getErrorMessage());
	}

}
